﻿using System;

namespace AreaCalculation
{
    /// <summary>
    /// Class used to calculate the areas of Square,Circle and Rectangle.
    /// </summary>
    public class AreaCalculation
    {
        /// <summary>
        /// length and breath values are taken as float
        /// </summary>
        private float length, breath;

        /// <summary>
        /// Main fuction to calculate 
        /// areas of Square, Circle and Rectangle.
        /// </summary>
        public static void Main()
        {
            AreaCalculation obj = new AreaCalculation(); //Creating object 
            obj.Square();                                //Calling the functions
            obj.Circle();
            obj.Rectangle();
            Console.ReadKey();   //To dipslay the console window output until user press any key
        }

        /// <summary>
        /// Getting Side of square from user
        /// Calculating the area
        /// Displaying to the user
        /// </summary>
        public void Square() 
        {
            Console.WriteLine("Enter the side of square :");
            try
            {
                length = float.Parse(Console.ReadLine());                   //Getting length from the user
                Console.WriteLine("Area of Square : " + length * length);  //Displaying result to user
            }
            catch
            { 
                Console.WriteLine("Invalid input.!! Try again with numbers");  //While exception occurs
            }                     
        }

        /// <summary>
        /// Getting radius of circle from user
        /// Calculating the area
        /// Displaying to the user
        /// </summary>
        public void Circle()
        {
            Console.WriteLine("Enter the radius of circle :");
            try
            {
                length = float.Parse(Console.ReadLine());                           //Getting radius from the user
                Console.WriteLine("Area of circle : " + Math.PI * length * length);    //Displaying result to user
            }
            catch
            {
                Console.WriteLine("Invalid input.!! Try again with numbers");  //While exception occurs
            }
        }

        /// <summary>
        /// Getting length and breath from user
        /// Calculating the area
        /// Displaying to the user
        /// </summary>
        public void Rectangle()
        {
            Console.WriteLine("Enter the length and breath of rectangle :");
            try
            {
                length = float.Parse(Console.ReadLine());
                breath = float.Parse(Console.ReadLine());                         //Getting length and breadth from the user
                Console.WriteLine("Area of rectangle : " + length * breath);        //Displaying result to user
            }
            catch
            {
                Console.WriteLine("Invalid input.!! Try again with numbers");  //While exception occurs
            }                
        }
    }
}
