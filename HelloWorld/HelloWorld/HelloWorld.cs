﻿using System;

namespace HelloWorld
{
    /// <summary>
    /// This class just contains main method to print Hello World
    /// </summary>
    class HelloWorld
    { 
        /// <summary>
        /// Main method prints Hello World
        /// </summary>
        public static void Main()
        {
            Console.WriteLine("Hello World..!!!!"); // Displaying Hello world to user
            Console.ReadKey();                      //Readkey() holds the console window until the user presses any key
        }
    }
}
